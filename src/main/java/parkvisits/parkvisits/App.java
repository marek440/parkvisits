package parkvisits.parkvisits;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class App {

	public static void main(String[] args) {
		if (!inputIsValid(args)) {
			System.out.println("Could not find the specified file!");
		} else {
			try {
				processVisits(args[0]);
			} catch (Exception e) {
				System.out.println("Error occurred: " + e.getMessage());
			}
		}
	}

	private static void processVisits(String path) throws IOException {
		List<DailyVisitCount> visitsCounts = readVisitCountsFromFile(path);
		DailyVisitCount maxVisitCount = findMaxVisitCount(visitsCounts);
		sortVisitsByCounts(visitsCounts);

		System.out.println("The maximum visit count is:");
		printVisitCount(maxVisitCount);

		System.out.println("The visits ordered by count are the following:");
		printVisitCounts(visitsCounts);
	}

	private static void printVisitCounts(List<DailyVisitCount> visitsCounts) {
		for (DailyVisitCount visitCount : visitsCounts) {
			printVisitCount(visitCount);
		}
	}

	private static void printVisitCount(DailyVisitCount visitCount) {
		System.out.println(String.format("Visit at %s: %d", visitCount.getDate(), visitCount.getCount()));
	}

	private static void sortVisitsByCounts(List<DailyVisitCount> visitsCounts) {
		visitsCounts.sort(new Comparator<DailyVisitCount>() {
			public int compare(DailyVisitCount vc1, DailyVisitCount vc2) {
				if (vc1.getCount() < vc2.getCount()) {
					return -1;
				}
				if (vc1.getCount() > vc2.getCount()) {
					return 1;
				}
				return 0;
			}
		});
	}

	private static DailyVisitCount findMaxVisitCount(List<DailyVisitCount> visitsCounts) {
		DailyVisitCount maxVisitCount = null;
		for (DailyVisitCount dailyVisitCount : visitsCounts) {
			if (maxVisitCount == null) {
				maxVisitCount = dailyVisitCount;
			} else {
				if (maxVisitCount.getCount() < dailyVisitCount.getCount()) {
					maxVisitCount = dailyVisitCount;
				}
			}
		}
		return maxVisitCount;
	}

	private static List<DailyVisitCount> readVisitCountsFromFile(String filePath) throws IOException {
		List<DailyVisitCount> visitCount = new ArrayList<DailyVisitCount>();
		List<String> lines = Files.readAllLines(Paths.get(filePath));
		for (String line : lines) {
			visitCount.add(new DailyVisitCount(line));
		}
		return visitCount;
	}

	private static boolean inputIsValid(String[] args) {
		if (args == null || args.length == 0) {
			return false;
		}
		return Files.exists(Paths.get(args[0]));
	}
}
