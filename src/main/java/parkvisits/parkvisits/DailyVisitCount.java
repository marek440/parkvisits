package parkvisits.parkvisits;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DailyVisitCount {
	
	private Date date;
	private int count;
	
	public DailyVisitCount(String line) {
		String[] properties = line.split(", ");
		try {
		this.setDate(this.fromStringToDate(properties[0]));
		this.setCount(Integer.parseInt(properties[1]));
		} catch (ParseException e) {
			throw new RuntimeException("Cold not instantiate the visit line: " + line);
		}
	}
	
	public Date getDate() {
		return date;
	}
	
	public void setDate(Date date) {
		this.date = date;
	}
	
	public int getCount() {
		return count;
	}
	
	public void setCount(int count) {
		this.count = count;
	}
	
	private Date fromStringToDate(String dateString) throws ParseException {
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
		return dateFormatter.parse(dateString);	
	}
}
